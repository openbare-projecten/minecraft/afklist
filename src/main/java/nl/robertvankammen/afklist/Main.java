package nl.robertvankammen.afklist;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

import static java.lang.System.currentTimeMillis;

public class Main extends JavaPlugin implements Listener {

    private static final String COMMAND_STRING = "afklist";
    private static final HashMap<Player, Long> timeLastInteracted = new HashMap<>();


    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase(COMMAND_STRING)) {
            int timeMinutesWait = 5;
            if (args.length == 1) {
                try {
                    timeMinutesWait = Integer.parseInt(args[0]);
                } catch (NumberFormatException ex) {
                    sender.sendMessage(args[0] + " is not a valid number");
                    return false;
                }
            }
            int finalTimeMinutesWait = timeMinutesWait;
            sender.sendMessage("Afk list:");
            timeLastInteracted.forEach((player, timeMillis) -> {
                long time = calculateMinutesUntilNow(timeMillis);
                if (time >= finalTimeMinutesWait) {
                    sender.sendMessage(player.getDisplayName() + " is afk for " + time + " minutes");
                }
            });
            return true;
        }
        return false;
    }

    private long calculateMinutesUntilNow(long oldTime) {
        return (currentTimeMillis() - oldTime) / 1000 / 60;
    }


    @EventHandler(priority = EventPriority.LOWEST)
    public void onMove(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        timeLastInteracted.remove(player);
        timeLastInteracted.put(player, currentTimeMillis());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent e) {
        timeLastInteracted.put(e.getPlayer(), currentTimeMillis());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onQuit(PlayerQuitEvent e) {
        timeLastInteracted.remove(e.getPlayer());
    }
}
